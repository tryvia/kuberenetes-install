This is a customized part of the code taken from [github.com/ReSearchITEng/kubeadm-playbook](https://github.com/ReSearchITEng/kubeadm-playbook.git) repository.

This playbook is assumed to be executed on the Kubernetes master node with the following command:

```
#!bash

ansible-playbook -i "localhost," -c local site.yml
```